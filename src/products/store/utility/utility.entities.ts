// array to entity structure
export function arrayToEntities<T>(
  models: T[],
  keyResolver: (model: T) => number,
  initialEntities: { [id: number]: T }
): { [id: number]: T } {
  return models.reduce(
    (entities: { [id: number]: T }, model: T) => {
      return {
        ...entities,
        [keyResolver(model)]: model
      };
    },
    {
      ...initialEntities // initial value for reduce
    }
  );
}

// entity structure to array
export function entitiesToArray<T>(entities: { [id: number]: T }): T[] {
  return Object.keys(entities).map(id => entities[parseInt(id, 10)]);
}
